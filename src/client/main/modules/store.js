import {
  SimpleStore,
  loggerPlugin,
  localStoragePlugin,
  // windowSizePlugin,
} from "@ombiel/aek-lib/simple-store";

import {
  request
} from "@ombiel/aek-lib";

import {
  has,
  get
} from "lodash";

import moment from "moment";
import Config from "../../config";

export class Store extends SimpleStore {
  constructor() {
    let plugins = [];
    plugins.push(loggerPlugin());
    // plugins.push(windowSizePlugin("user.windowDimensions"));
    if (Config.storageEnabled && Config.storageKey && Config.storageKey.length > 0) {
      plugins.push(localStoragePlugin(Config.storageKey));
    }

    super({
      initialState: {
        rail: {
          railData: [],
          railError: false,
          $$$railLoading: true
        },
        tube: {
          tubeData: [],
          tubeError: false,
          $$$tubeLoading: true
        }
      },
      plugins: plugins,
    });
  }
}

const store = new Store();
export { store };

// this is the function used to manipulate this store, called by the getViewData method
function dispatchRailData(views, error = false, loading = false) {
  store.dispatch({
    name: "RAIL_STATUS",
    path: "rail",
    extend: {
      railData: views,
      railError: error,
      $$$railLoading: loading
    }
  });
}

export function fetchRailStatus() {
  dispatchRailData(null, false, true);
  request.action("get-rail").end((error, response) => {
    if (!error && response && response.body && response.body.TOC) {
      dispatchRailData(response.body.TOC, false, false);
    }
    else {
      dispatchRailData(null, true, false);
    }
  });
}

function dispatchTubeData(views, error = false, loading = false) {
  store.dispatch({
    name: "TUBE_STATUS",
    path: "tube",
    extend: {
      tubeData: views,
      tubeError: error,
      $$$tubeLoading: loading
    }
  });
}

export function fetchTubeStatus() {
  dispatchTubeData(null, false, true);
  request.action("get-tube").end((error, response) => {
    if (!error && response && response.body && response.body.LineStatus) {
      dispatchTubeData(response.body.LineStatus, false, false);
    }
    else {
      dispatchTubeData(null, true, false);
    }
  });
}
