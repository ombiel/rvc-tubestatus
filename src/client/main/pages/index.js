import React from "react";
import {
  BannerHeader,
  BasicSegment,
  Item,
  Listview,
  Page
} from "@ombiel/aek-lib";
import {
  map
} from "lodash";
import { store } from "../modules/store";
import Config from "../../config";

// eslint-disable-next-line react/prefer-stateless-function
export default class IndexPage extends React.Component {
  render() {

    const { $$$railLoading } = store.get("rail");
    const { $$$tubeLoading } = store.get("tube");

    return (
      <Page>
        <BannerHeader theme="prime">{Config.indexPage.title}</BannerHeader>
        <BasicSegment nopadding loading={$$$railLoading && $$$tubeLoading}>
          <Listview items={Config.indexPage.list} />
        </BasicSegment>
      </Page>
    );
  }
}
