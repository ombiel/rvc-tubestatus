import React from "react";
import {
  BasicSegment,
  InfoMessage,
  ListviewItem as Item,
  Listview,
  Page
} from "@ombiel/aek-lib";
import {
  filter,
  forEach,
  isArray
} from "lodash";
import { store } from "../modules/store";

// eslint-disable-next-line react/prefer-stateless-function
export default class RailPage extends React.Component {
  render() {

    const { $$$railLoading, railError, railData } = store.get("rail");

    if (!$$$railLoading && railData) {

      // var data = statusData;
      var data = filter(railData, (line) => { return (line.TocCode === "GN" || line.TocCode === "TL"); });
      // console.log(data);
      
      return (
        <Page>
          <BasicSegment nopadding>
            <Listview
              items={data}
              itemFactory={line => {
                let text;
                let subtext;
                let icon;
                let color;

                if (line.Status === "Good service") {
                  text = line.Status;
                  icon = "checkmark";
                  color = "#045004";
                }
                else if (line.Status === "Minor delays on some routes") {
                  text = line.Status;
                  subtext = "";
                  if (isArray(line.ServiceGroup)) {
                    forEach(line.ServiceGroup, group => {
                      subtext += group.GroupName + ", ";
                    });
                  }
                  else {
                    subtext += line.ServiceGroup.GroupName + ", ";
                  }
                  icon = "warning circle";
                  color = "#ead217";
                }
                else if (line.Status === "Custom") {
                  text = line.StatusDescription;
                  icon = "warning sign";
                  color = "#bd0303";
                }
                return (
                  <Item icon={icon} key={line.TocCode} thumbSize="big" iconProps={{ style: { color: color } }}>
                    <h4>{line.TocName}</h4>
                    <p>{text}</p>
                    {subtext
                      ? <p>{subtext.substring(0, subtext.length - 2)}</p>
                      : null}
                  </Item>
                );
              }}
            />
          </BasicSegment>
        </Page>
      );
    }
    else {
      return (
        <Page>
        <BasicSegment>
          <InfoMessage>Cannot retrieve National Rail status data.</InfoMessage>
        </BasicSegment>
        </Page>
      )
    } 
  }
}
