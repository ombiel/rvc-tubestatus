import React from "react";
import {
  BasicSegment,
  InfoMessage,
  ListviewItem as Item,
  Listview,
  Page
} from "@ombiel/aek-lib";
import {
  filter,
  forEach,
  isArray
} from "lodash";
import { store } from "../modules/store";

// eslint-disable-next-line react/prefer-stateless-function
export default class TubePage extends React.Component {
  render() {

    const { $$$tubeLoading, tubeError, tubeData } = store.get("tube");

    if (!$$$tubeLoading && tubeData) {

      return (
        <Page>
          <BasicSegment nopadding>
            <Listview
              items={tubeData}
              itemFactory={line => {
                let text;
                let subtext;
                let icon;
                let color;
                let lineColor;

                switch (line.Line["@attributes"].Name) {
                  case "Bakerloo":
                    lineColor = "#894E24";
                    break;
                  case "Central":
                    lineColor = "#DC241F";
                    break;
                  case "Circle":
                    lineColor = "#FFCE00";
                    break;
                  case "District":
                    lineColor = "#007229";
                    break;
                  case "Hammersmith and City":
                    lineColor = "#D799AF";
                    break;
                  case "Jubilee":
                    lineColor = "#868F98";
                    break;
                  case "Metropolitan":
                    lineColor = "#751056";
                    break;
                  case "Northern":
                    lineColor = "#000";
                    break;
                  case "Piccadilly":
                    lineColor = "#0019A8";
                    break;
                  case "Victoria":
                    lineColor = "#00A0E2";
                    break;
                  case "Waterloo and City":
                    lineColor = "#76D0BD";
                    break;
                  case "Overground":
                    lineColor = "#F86C00";
                    break;
                  case "TfL Rail":
                    lineColor = "#000";
                    break;
                  case "DLR":
                    lineColor = "#00BBB4";
                    break;
                  case "Trams":
                    lineColor = "#000";
                    break;
                  default:
                    lineColor = "#000";
                    break;
                }

                if (line.Status["@attributes"].Description === "Good Service") {
                  text = line.Status["@attributes"].Description;
                  icon = "checkmark";
                  color = "#045004";
                }
                else if (line.Status["@attributes"].Description === "Minor Delays") {
                  text = line.Status["@attributes"].Description;
                  subtext = line["@attributes"].StatusDetails;
                  icon = "warning circle";
                  color = "#ead217";
                }
                else if (line.Status["@attributes"].Description === "Severe Delays") {
                  text = line.Status["@attributes"].Description;
                  subtext = line["@attributes"].StatusDetails;
                  icon = "warning sign";
                  color = "#bd0303";
                }
                return (
                  <Item icon={icon} key={line["@attributes"].ID} thumbSize="big" iconProps={{ style: { color: color } }}>
                    <h4 style={{color: lineColor, margin: "0"}}>{line.Line["@attributes"].Name}</h4>
                    <b style={{margin: "0"}}>{text}</b>
                    {subtext
                      ? <p>{subtext}</p>
                      : null}
                  </Item>
                );
              }}
            />
          </BasicSegment>
        </Page>
      );
    }
    else {
      return (
        <Page>
          <BasicSegment>
            <InfoMessage>Cannot retrieve tube status data.</InfoMessage>
          </BasicSegment>
        </Page>
      )
    }
  }
}
