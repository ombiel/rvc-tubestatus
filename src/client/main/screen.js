import React from "react";
import {
  AekReactRouter,
  Container,
  RouterView,
  VBox,
  BannerHeader,
  BasicSegment,
  ErrorMessage,
} from "@ombiel/aek-lib";

import { store, fetchTubeStatus, fetchRailStatus } from "./modules/store";
import IndexPage from "./pages/index";
import RailPage from "./pages/rail";
import TubePage from "./pages/tube";

const router = new AekReactRouter();

export default class Screen extends React.Component {
  componentDidMount() {
    store.on("change", () => {
      this.forceUpdate();
    });

    // we use this to trigger a forceUpdate on router paging, allowing us to capture currentPath
    router.addRoute("*", (ctx, next) => {
      next();
      this.forceUpdate();
    });

    // endsWith fallback for IE - https://caniuse.com/#search=endswith
    if (!String.prototype.endsWith) {
      String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
      };
    }

    fetchTubeStatus();
    fetchRailStatus();
  }

  componentWillUnmount() {
    store.off("change");
  }

  render() {
    return (
      <Container>
        <VBox>
          <RouterView router={router}>
            <IndexPage path="/" />
            <RailPage path="/rail" />
            <TubePage path="/tube" />
          </RouterView>
        </VBox>
      </Container>
    );

  }

}
